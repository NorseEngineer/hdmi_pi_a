EESchema Schematic File Version 2
LIBS:hdmi
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L HDMI U1
U 1 1 59336BB6
P 5450 2750
F 0 "U1" H 5750 3050 60  0000 C CNN
F 1 "HDMI" H 5500 3050 60  0000 C CNN
F 2 "hdmi:HDMI_Female" H 5450 2750 60  0001 C CNN
F 3 "" H 5450 2750 60  0001 C CNN
	1    5450 2750
	-1   0    0    -1  
$EndComp
$Comp
L HDMI U2
U 1 1 59336C07
P 7450 2750
F 0 "U2" H 7750 3050 60  0000 C CNN
F 1 "HDMI" H 7500 3050 60  0000 C CNN
F 2 "hdmi:HDMI_Male" H 7450 2750 60  0001 C CNN
F 3 "" H 7450 2750 60  0001 C CNN
	1    7450 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 2600 7450 2600
Wire Wire Line
	7450 2700 5450 2700
Wire Wire Line
	5450 2800 7450 2800
Wire Wire Line
	7450 2900 5450 2900
Wire Wire Line
	5450 3000 7450 3000
Wire Wire Line
	7450 3100 5450 3100
Wire Wire Line
	5450 3200 7450 3200
Wire Wire Line
	7450 3300 5450 3300
Wire Wire Line
	5450 3400 7450 3400
Wire Wire Line
	5450 3500 7450 3500
Wire Wire Line
	7450 3600 5450 3600
Wire Wire Line
	5450 3700 7450 3700
Wire Wire Line
	5450 3800 7450 3800
Wire Wire Line
	5450 3900 7450 3900
Wire Wire Line
	5450 4000 7450 4000
Wire Wire Line
	5450 4100 7450 4100
Wire Wire Line
	5450 4200 7450 4200
Wire Wire Line
	5450 4300 7450 4300
Wire Wire Line
	5450 4400 7450 4400
Wire Wire Line
	5450 4500 7450 4500
Wire Wire Line
	5450 4600 7450 4600
Text Notes 5100 2300 0    60   ~ 0
Female
Text Notes 7500 2300 0    60   ~ 0
Male
$EndSCHEMATC
